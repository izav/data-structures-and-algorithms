﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LinkList
{
    class Program
    {
        static void Main(string[] args)
        {
        Node node = new Node(2);
		node.next = new Node(1);
		node.next.next = new Node(3);
		node.next.next.next = new Node(7);
        node.next.next.next.next = new Node(8);
        node.next.next.next.next.next= new Node(7);
        node.next.next.next.next.next.next = new Node(1);
        node.next.next.next.next.next.next.next = new Node(2);
        node.next.next.next.next.next.next.next.next = new Node(12);

        Node node2 = new Node(4);
        node2.next = new Node(8);
        node2.next.next = new Node(9);
        node2.next.next.next = new Node(44);
        node2.next.next.next.next = new Node(8);
        node2.next.next.next.next.next = new Node(5);
        node2.next.next.next.next.next.next = new Node(2);
        node2.next.next.next.next.next.next.next = new Node(22);

        //Sorted linked list1
        Node sortednode1 = new Node(4);
        sortednode1.next = new Node(6);
        sortednode1.next.next = new Node(8);
        sortednode1.next.next.next = new Node(12);
        sortednode1.next.next.next.next = new Node(55);

        //Sorted linked list2
        Node sortednode2 = new Node(2);
        sortednode2.next = new Node(8);
        sortednode2.next.next = new Node(8);
        sortednode2.next.next.next = new Node(10);
        sortednode2.next.next.next.next = new Node(55);
        sortednode2.next.next.next.next.next = new Node(56);

        //Circular list
        Node loopnode = new Node(2);
        loopnode.next = new Node(8);
        var loopstart = loopnode.next.next = new Node(88888);
        loopnode.next.next.next = new Node(10);
        loopnode.next.next.next.next = new Node(55);
        loopnode.next.next.next.next.next = loopstart;

        //T(Y) list
        Node y_node1 = new Node(12);
        y_node1.next = new Node(44);
        Node intersect_node = y_node1.next.next = new Node(12);
        y_node1.next.next.next = new Node(10);
        y_node1.next.next.next.next = new Node(33);
        y_node1.next.next.next.next.next = new Node(4);

        Node y_node2 = new Node(8);
        y_node2.next = new Node(5);
        y_node2.next.next = new Node(2);
        y_node2.next.next.next = new Node(0);
        y_node2.next.next.next.next = intersect_node;


        // TEST-------------------        
        Console.WriteLine("Original\n{0}",node.ToString());

        // isPolindrome-------
        Console.WriteLine("\nIs polidrome? {0}\n",Node.isPolindrome(node));
        
        //Nth element ---------  
        var result1 = Node.nthElementAtLinkList(node, 3);
        var result2 = Node.nthElementAtLinkListIteration(node, 4);
        Console.Write("Nth element recurcive:\nOriginal {0}th element = {1}\n", 3, 
                        result1 == null ? "no data" : result1.data.ToString());
        Console.Write("\nNth element iterative:\nOriginal {0}th element = {1}\n", 4, 
                        result2 == null ? "no data" : result2.data.ToString());       

        //removeDublicates------
        Console.WriteLine("\nRemove dublicates");
        Node.removeDublicates(node);
        Console.WriteLine(node.ToString());

        //DeleteNodeByValue------
        Console.WriteLine("\nDelete value 7");
        Node.DeleteNodeByValue(node, 7);
        Console.WriteLine(node.ToString());
                   
        //Reverse node
        node = Node.Reverse(node);
        Console.Write("\nPrint reverse recursive");
        Node.PrintReverse(node, "");

        //Original
        Console.Write("\n\nOriginal:\n{0}\n", node.ToString());
               
        //Rotate nodes
        node = Node.RotateNodes(node, 3);
        Console.Write("\nRotate node 3:\n{0}", node.ToString());
        Console.WriteLine();

         //Interweave node
         Node.Interweave(node);
         Console.Write("\nInterweave:\n{0}", node.ToString());
         Console.WriteLine();

         //Find middle node recursive & iterative 
         Console.WriteLine();
         Node.FindMiddleNodeRecursive(node);
         Console.WriteLine();
         Node.FindMiddleNodeIterative(node);

         //Insert
         Console.Write("Original:\n{0}\n", node.ToString());
         node = Node.InsertAtHead(node, 32);
         Node.InsertAtTail(node, 78);
         Console.WriteLine("\nInsert at head value = 32, at tail value = 78:\n{0}", node.ToString());
         node = Node.InsertAtNthPosition(node, 8, 3);
         Console.WriteLine("\nInsert value = 8 at index = 3:\n{0}", node.ToString());


         //Intersection 
         var node3 = Node.Intersection(node, node2);
         Console.Write("\nIntersection list1 & list2\nList1:\n{0}", node.ToString());
         Console.Write("\nList2:\n{0}", node2.ToString());
         Console.Write("\nIntersection:\n{0}",  (node3 == null)? "Null" : node3.ToString());

         //MergeSortedListsIterative 
         Console.Write("\n\nMerge sorted linked lists\nList1: {0}", sortednode1.ToString());
         Console.Write("\nList2: {0}", sortednode2.ToString());
         var node4 = Node.MergeSortedListsRecursive(Node.DeepCopy(sortednode1), 
                        Node.DeepCopy(sortednode2));
         var node5 = Node.MergeListsIterative(Node.DeepCopy(sortednode1), 
                       Node.DeepCopy(sortednode2));
         Console.Write("\nMerge recurcive:\n{0}", node4.ToString());
         Console.Write("\nMerge iterative:\n{0}", node5.ToString());

         //Circular  list 
         Console.WriteLine();
         var startnode = Node.IsCircular(loopnode);

         Console.Write("\nCircular linked list:\n{0}", loopnode.ToString(startnode, startnode, "*"));
         Console.Write("\nHasCycle:{0}\nCycle starts at node with value {1}", startnode != null,
                          startnode!= null ? startnode.data.ToString(): "no value");

         //T(Y) list 
         Console.WriteLine();
         var intersection = Node.FindMergeNode(y_node1, y_node2);
         Console.Write("\nT(Y) linked list:\nhead1:\n{0}\nhead2:\n{1}", 
                             y_node1.ToString(intersection, null), 
                             y_node2.ToString(intersection, null));
         Console.Write("\n{0}", intersection == null? "no intersection": 
             "Interesect value: " +intersection.data.ToString());

         Console.WriteLine();
         Console.ReadKey();
        }
     }

    
     public class Node
     {
            public int data;
            public Node next = null;
         
            public Node(int k)
            {
                data = k;
            }

            public Node(Node copy)
            {
                data = copy.data;
              
                if (copy.next != null )                  
                    next = new Node(copy.next);                 
                else
                    next = null;
            }

            override public string ToString()
            {
               
                Node current = this;
                string output = "";
                while (current != null)
                {
                    output += current.data + "->";
                    current = current.next;
                }
                return output + "Null";
            }

            //Circular list: 5-> 6->[2->4->]*, T(Y) head1:  3->2->[7->2->Null]  head2: 5->9->[7->2->Null]
            public string ToString(Node node1, Node node2, string symbol = "")
            {

                Node current = this;
                string output = "";
                while (current != node1)
                {
                    output += current.data + "->";
                    current = current.next;
                }
                output += "[" + node1.data + "->";
                current = node1.next;
                while (current != node2)
                {
                    output += current.data + "->";
                    current = current.next;
                }

                return output += symbol == "" ? "Null]" : "]" + symbol; 
            }

           

         
            public static void removeDublicates(Node head)
            {
                if (head == null) return;
                Node outer = head;
                while (outer != null)
                {
                    Node inner = outer;
                    while(inner.next != null)
                    {
                        if (outer.data == inner.next.data)
                            inner.next = inner.next.next;
                        else
                            inner = inner.next;
                    }
                    outer = outer.next;
                }  
             }



           public static Node nthElementAtLinkList(Node head, int k)
           {              
               if (head == null || k < 0) return null;                             
               else if (k == 0) return head;
               else return nthElementAtLinkList(head.next, k - 1); ;
           }

           
           public static Node nthElementAtLinkListIteration(Node head, int k)
           {               
               if (head == null || k < 0 ) return null;

               Node current = head;
               for (int i = 0; i < k ; i++)
               {
                   if (current.next == null) return null;
                   current = current.next;
               }
               return current;
  
         }

           public static Node DeepCopy (Node head)
           {
               Node newHead = new Node (head);
               return newHead;
           }


           public static Node Intersection(Node node1, Node node2)
           {              
               Node head = null;
               Node tail = null;
               var node = node2;
               while (node1 != null)
               {
                   node2 = node;
                   while (node2 != null)
                   {
                       if (node1.data == node2.data) {
                           var node3 = head;
                           while (node3 != null) {
                               if (node3.data == node1.data) break;
                               node3 = node3.next;
                           }
                           //if new value
                           if (node3 == null) {
                               Node temp = new Node(node1.data);
                               if (tail == null) {
                                   head = tail = temp;
                               }
                               else  {
                                  tail.next = temp;
                                  tail = temp;
                               }
                           }                   
                       }
                       node2 = node2.next;                      
                   }
                   node1 = node1.next;
               }
               return head;
           }



           public static Node InsertAtTail(Node head, int value)
           {
               Node current = head;
               while (current != null && current.next != null)
               { 
                   current = current.next; 
               }
               Node n = new Node(value);
               if (head == null) head = current = n;
               else current.next = n;
               return head;
           }


           public static Node InsertAtHead(Node head, int value)
           {
               Node node = new Node(value);              
               if (node != null) node.next = head; 
               return node;
           }


           public static Node InsertAtNthPosition(Node head, int value, int index)
           {             
               Node node = new Node(value);
               if (index == 0 && head == null)
                   return node;
               else if (index == 0)
               { node.next = head; return node; }
               Node current = head;

               int i = 1;
               while (current != null && current.next != null && index != i)
               {
                   current = current.next;
                   i++;
               }
               node.next = current.next;
               current.next = node;

               return head;
           }



             // lists must be sorted
           public static Node MergeSortedListsRecursive(Node node1, Node node2)
           {
              if (node1 == null) return node2;
              if (node2 == null) return node1;

              Node node3 = null;
              if (node1.data < node2.data)
              {
                  node3 = node1;
                  node3.next = MergeSortedListsRecursive(node1.next, node2);
              } else {
                  node3 = node2;
                  node3.next = MergeSortedListsRecursive(node1, node2.next);               
              }
              return node3;
            }


            public static Node MergeListsIterative(Node list1, Node list2) {
              if (list1 == null) return list2;
              if (list2 == null) return list1;
 
              Node head;
              if (list1.data < list2.data) {
                head = list1;
              } else {
                head = list2;
                list2 = list1;
                list1 = head;
              }
              while(list1.next != null && list2 != null) {
                if (list1.next.data <= list2.data) {
                  list1 = list1.next;
                } else {
                  Node tmp = list1.next;
                  list1.next = list2;
                  list2 = tmp;
                }
              } 
              if (list1.next == null) list1.next = list2;
              return head;
            }


            static int length = 0;
            static int reverseIndex = 0;

            public static void FindMiddleNodeRecursive(Node node)
            {
                if (node != null)
                {
                    length++;
                    FindMiddleNodeRecursive(node.next);
                    reverseIndex++;
                }

                if (reverseIndex * 2 == length ||
                    reverseIndex * 2 == length + 1)
                    Console.WriteLine("Find middle node recurcive\nmiddle node value is {0}", node.data);
            }


            public static void FindMiddleNodeIterative(Node node)
            {
                Node n1 = node;
                Node n2 = node;
                while (n2.next != null && n2.next.next != null)
                {
                    n1 = n1.next;
                    n2 = n2.next.next;
                }

                Console.WriteLine("Find middle node iterative\nmiddle node value is {0}", n1.data);
            }


            static public Node DeleteNodeByValue(Node head, int data)
            {
                Node n = head;
                if (n.data == data)
                    return n.next;

                while (n.next != null)
                {
                    if (n.next.data == data)
                    {

                        n.next = n.next.next;
                        return head;
                    }
                    n = n.next;
                }
                return head;
            }

            public static Node Reverse(Node node)
            {
                if (node == null || node.next == null)
                    return node;

                Node rest = Reverse(node.next);
                node.next.next = node;
                node.next = null;
                return rest;
            }

            public static void PrintReverse(Node head, string symbol)
            {
                if (head == null) {
                    Console.Write("\nNull<-"); 
                    return; 
                }
                else PrintReverse(head.next, "<-");

                Console.Write(head.data + symbol); 
            }


            public static void Interweave(Node node)
            {
                Node mid = FindMid(node);

                Node secondhalf = Reverse(mid.next);
                Node firsthalf = node;

                mid.next = null;
                node = MergeSortedListsRecursive(firsthalf, secondhalf);
            }


            private static Node FindMid(Node L)
            {
                Node fast = L;
                Node slow = L;
                while (fast != null && fast.next != null && slow != null)
                {
                    fast = fast.next.next;
                    slow = slow.next;
                }
                return slow;
            }


            public static Node RotateNodes(Node node, int k)
            {
                Node current = node;
                Node newStart;

                while (k > 1)
                {
                    current = current.next;
                    k--;
                }
                newStart = current.next;
                current.next = null;
                current = newStart;

                while (current.next != null)
                    current = current.next;

                current.next = node;
                return newStart;
            }


            // Is Loop find begining
            //--------------------------------
            // (A+B)*2 = A + B + Cicle <=> A+B = Cicle( circle*n)
            // A = Cicle -  B ( poit of intersection)
            public static Node IsCircular(Node head)
            {
    
                Node turtle = head;
                Node rabbit = head;
                while (turtle != null && rabbit != null)
                {
                    turtle = turtle.next;
                    rabbit = rabbit.next.next;
                    if (turtle == rabbit)
                        break;
                }

                if (rabbit == null || rabbit.next == null)
                    return null; // no loop

                turtle = head;
                while (turtle != rabbit)
                {
                    turtle = turtle.next;
                    rabbit = rabbit.next;
                }

                return rabbit;
            }

            //T(Y) linked list
            public static Node FindMergeNode(Node head1, Node head2)
            {              
                int length1 = 0;
                int length2 = 0;
                Node node1 = head1;
                Node node2 = head2;
                Node current = node1;
                while (current != null)
                {
                    length1++;
                    current = current.next;
                }

                current = node2;
                while (current != null)
                {
                    length2++;
                    current = current.next;
                }
                
                if (length1 > length2)
                {
                    for (int i = 0; i < length1 - length2; i++)
                        node1 = node1.next;
                }
                else if (length2 > length1)
                {
                    for (int i = 0; i < length2 - length1; i++)
                        node2 = node2.next;
                }
                
                while (node1 != node2 && node1 != null && node2 != null)
                {
                    node1 = node1.next;
                    node2 = node2.next;
                }
               
                return node1;
            }



            public static bool isPolindrome(Node head)
            {
                Node turtle = head;
                Node rabbit = head;
                Stack<int> stack = new Stack<int>();

                //find middle
                while (rabbit != null && rabbit.next != null)
                {
                    stack.Push(turtle.data);
                    rabbit = rabbit.next.next;
                    turtle = turtle.next;        
                }

                // odd
                if (rabbit != null)
                {
                    turtle = turtle.next;
                }   
                while (turtle != null)
                {
                    if (turtle.data != stack.Pop())
                         return false;

                    turtle = turtle.next;
                }

                return true;
            }
      }
    
}

