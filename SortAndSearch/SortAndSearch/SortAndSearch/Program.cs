﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SortAndSearch
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] arr1 = { 12, 3, 5, 8, 12, 44, 9, 3, 45, 2, 77 };
            Console.Write("BubbleSort: before");
            PrintArray(arr1);
            BubbleSort(arr1);
            Console.Write("BubbleSort: after");
            PrintArray(arr1);

            int[] arr2 = { 2, 13, 25, 18, 1, 44, 9, 3, 5, 7 };
            Console.Write("\nInsertionSort: before");
            PrintArray(arr2);
            InsertionSort(arr2);
            Console.Write("InsertionSort: after");
            PrintArray(arr2);


            int[] arr3 = { 9, 3, 45, 2, 77, 12, 3, 5, 8, 12, 44, 1};
            Console.Write("\nSelectionSort: before");
            PrintArray(arr3);
            SelectionSort(arr3);
            Console.Write("SelectionSort: after");
            PrintArray(arr3);

            int[] arr4 = { 4, 45, 24, 8, 3, 25, 12, 23, 15, 18, 6, 32};         
            Console.Write("\nQuickSort: before");
            PrintArray(arr4);
            QuickSort(arr4);
            Console.Write("QuickSort: after");
            PrintArray(arr4);

            Console.Write("\nMergeSort: before\n");
            Console.Write("array1: ");
            PrintArray(arr2);
            Console.Write("array2: ");
            PrintArray(arr4);
            var arr5 = MergeSort(arr2, arr4);
            Console.Write("MergeSort: after");
            PrintArray(arr5);
                 
            Console.Write("\nBinarySearch in array:");
            PrintArray(arr4);
            BinarySearch(arr4,25);
            Console.Write("for value = 23 index = " + BinarySearch(arr4, 23));
            Console.Write("\nfor value = 45 index = " + BinarySearch(arr4, 45));
            Console.WriteLine();
            Console.ReadKey();

        }

        public static void PrintArray(int[] arr) {
            Console.Write("\n");
            for (int i = 0; i < arr.Length; i++)
            {
                Console.Write(arr[i]);
                if (i < arr.Length-1) Console.Write(", ");
            }
            Console.Write("\n");
        }

        public static void BubbleSort(int[] arr)
        {

            bool swapped = true;
            int n = arr.Length;
            int j = 0;
            int tmp;

            while (swapped)
            {
                swapped = false;
                j++;
                for (int i = 0; i < n - j; i++)
                {
                    if (arr[i] > arr[i + 1])
                    {
                        tmp = arr[i];
                        arr[i] = arr[i + 1];
                        arr[i + 1] = tmp;
                        swapped = true;
                    }
                }

            }
        }



        public static void InsertionSort(int[] arr)
        {
            int i, j, tmp;
            int length = arr.Length;

            for (i = 1; i < length; i++)
            {
                j = i;
                while (j > 0 && arr[j - 1] > arr[j])
                {
                    tmp = arr[j];
                    arr[j] = arr[j - 1];
                    arr[j - 1] = tmp;
                    j--;
                }
            }
        }



        public static void SelectionSort(int[] arr)
        {
            int i, j, minIndex, tmp;
            int n = arr.Length;

            for (i = 0; i < n - 1; i++)
            {
                minIndex = i;

                for (j = i + 1; j < n; j++)
                    if (arr[j] < arr[minIndex])
                        minIndex = j;

                if (minIndex != i)
                {
                    tmp = arr[i];
                    arr[i] = arr[minIndex];
                    arr[minIndex] = tmp;
                }
            }
        }


      public static void QuickSort(int[] arr)
      {
              quickSort(arr, 0, arr.Length - 1);
      }


      private static void quickSort(int[] arr, int left, int right) {

              int i = left, j = right;
              int tmp;
              int pivot = arr[(left + right) / 2];

              /* partition */
              while (i <= j)
              {
                  while (arr[i] < pivot)  i++;
                  while (arr[j] > pivot) j--;

                  if (i <= j)
                  {
                      tmp = arr[i];
                      arr[i] = arr[j];
                      arr[j] = tmp;
                      i++;
                      j--;
                  }
              };

              if (left < j)
                  quickSort(arr, left, j);

              if (i < right)
                  quickSort(arr, i, right);
            
       }


      public static int BinarySearch(int[] arr, int value)
      {
          int left = 0;
          int right = arr.Length;

          while (left <= right)
          {
              int middle = (left + right) / 2;
              if (arr[middle] == value) return middle;
              else if (arr[middle] > value) right = middle - 1;
              else left = middle + 1;
          }
          return -1;
      }


      //arrays must be sorted
      public static int[] MergeSort(int[] A, int[] B)
      {
              int i, j, k;
              i = 0;
              j = 0;
              k = 0;
              int m = A.Length;
              int n = B.Length;
              int[] C = new int[m + n];

              while (i < m && j < n) {
                    if (A[i] <= B[j]) {
                          C[k] = A[i];
                          i++;
                    } else {
                        C[k] = B[j];
                        j++;
                    }
                    k++;
              }

              if (i < m)
              {
                  for (int p = i; p < m; p++)
                  {
                      C[k] = A[p];
                      k++;
                  }
              }
              else
              {
                  for (int p = j; p < n; p++)
                  {
                      C[k] = B[p];
                      k++;
                  }
              }
              return C;
        }

    }
}
