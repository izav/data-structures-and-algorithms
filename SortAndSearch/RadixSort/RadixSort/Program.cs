﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/*
 * LSD radix sorts typically use the following sorting order: short keys come before longer keys, 
 * and keys of the same length are sorted lexicographically. This coincides with the normal order 
 * of integer representations, such as the sequence 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11.
 * 
 * MSD radix sorts use lexicographic order, which is suitable for sorting strings, such as words, 
 * or fixed-length integer representations. A sequence such as "b, c, d, e, f, g, h, i, j, ba" 
 * would be lexicographically sorted as "b, ba, c, d, e, f, g, h, i, j"
 */

namespace RadixSort
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("LSD - normal order of integer representations\nbefore:");
            int[] arr1 = new int[] { 2, 15, 1, 13, -4 };
            Console.Write(string.Join(", ", arr1));
            SortLSD(arr1);
            Console.WriteLine("\nafter:");
            Console.Write(string.Join(", ", arr1));

            Console.Read();

        }

        //least significant digit (LSD)
        static void SortLSD(int[] arr)
        {
            int i, j;
            int[] tmp = new int[arr.Length];
            for (int shift = 31; shift > -1; --shift)
            {
                j = 0;
                for (i = 0; i < arr.Length; ++i)
                {
                    bool move = (arr[i] << shift) >= 0;
                    if (shift == 0 ? !move : move)  // shift the 0's to old's head
                        arr[i - j] = arr[i];
                    else                            // move the 1's to tmp
                        tmp[j++] = arr[i];
                }
                Array.Copy(tmp, 0, arr, arr.Length - j, j);
            }
        }

     

    }
}
