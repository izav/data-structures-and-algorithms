﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Array_PrintAllSubsets
{
    class Program
    {
        //Given a set of num­bers, print all the poss­si­ble sub­sets of it includ­ing empty set.
        static void Main(string[] args)
        {
            int[] arr = { 2, 4, 6 };
            Console.WriteLine("Array:\n{0}\n\nSubsets:", String.Join(", ", arr));           
            PrintSubSet(arr);
            Console.ReadKey();
        }
      
        public static void PrintSubSet(int[] arr)
        {            
            for (int i = 0; i <= arr.Length; i++)
            {
                bool[] ifPrint = new bool[arr.Length];
                PrintSubSet(arr, ifPrint, 0, i);
            }
        }

        
        public static void PrintSubSet(int[] nums, bool[] ifPrint, int start, int remain)
	   {		
		    if(remain==0)
		    {
			    Console.Write("{");			
			    for(int i = 0; i < ifPrint.Length;i++)
			    {
				    if(ifPrint[i])
					    Console.Write(nums[i] + (i== ifPrint.Length -1 ? "" :","));
			    }
                Console.Write("} ");
		    }
		    else
		    {			
			    if(start+remain>nums.Length);
			    else
			    {
				    for(int i=start; i<nums.Length;i++)
				    {					
					    if(!ifPrint[i])
					    {						
						    ifPrint[i] = true;						
						    PrintSubSet(nums, ifPrint, i+1, remain-1);						
						    ifPrint[i] = false;
					    }
				    }
			    }
		    }
	    }
    }
}
