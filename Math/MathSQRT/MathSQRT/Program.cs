﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MathSQRT
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("NewtonSqrt {0}" + getNewtonSqrt(99.044));
            Console.WriteLine("\nSqrt  {0}" + mySqrt(99.044));
            Console.ReadKey();
        }


       
        public static double getNewtonSqrt(double num)
        {
            double x = 1, newX, precision = 0.00000000001;
            while (true)
            {              
                newX = .5 * (x + (num / x));   
                if (Math.Abs(x - newX) < precision)
                    break;
                else x = newX;
            }
            return x;
        }


    
        public static double mySqrt(double a)
        {
           
            if (a < 0) return -1;
            if (a == 0 || a == 1) return a;

            double precision = 0.00001;
            double start = 0;
            double end = a;
            if (a < 1)
                end = 1;

            while (end - start > precision)
            {
                double mid = (start + end) / 2;
                double midSqr = mid * mid;
                if (midSqr == a) return mid;
                else if (midSqr < a) start = mid;
                else end = mid;
            }
            return (start + end) / 2;
        }
    }
}